import React, { useState, useEffect, FC } from 'react';
import InputMask, { Props as InputMaskProps } from 'react-input-mask';

import classes from 'utils/classNames';

import styles from './textfield.module.scss';

export interface TextFieldProps {
  id: string;
  Icon?: FC,
  type?: string;
  fieldName?: string;
  value?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  errors?: object;
  disabled?: boolean;
  mask?: InputMaskProps['mask']
}

export const TextField: FC<TextFieldProps> = ({ Icon, id, type, fieldName, value, onChange, errors, disabled, mask }) => {
  const [filled, setFilled] = useState(value ? true : false);

  useEffect(() => {
    setFilled(value ? true : false);
  }, [value]);

  const onFocus = () => {
    setFilled(true);
  };
  const onBlur = () => {
    setFilled(value ? true : false);
  };

  const getError = () => {
    if (!errors) return false;

    type Iterator = keyof typeof errors;

    return errors[id as Iterator];
  };

  return (
    <div className={classes(
      styles.formfield, styles.textfield,
      filled && styles.filled,
      getError() && 'error',
      Icon && 'icon'
    )}>
      <span className={styles["textfield__label-bg"]} >{fieldName}</span>
      <label
        htmlFor={id}
        className={styles.textfield__fieldname}
      >{fieldName}</label>

      {mask && <InputMask
        id={id}
        name={id}
        type={type}
        onFocus={onFocus}
        onBlur={onBlur}
        onChange={onChange}
        value={value ? value : ''}
        disabled={disabled}
        autoComplete={type == 'password' ? 'new-password' : undefined}
        mask={mask}
      />}

      {Icon && <Icon />}
      {!mask && <input
        id={id}
        name={id}
        type={type}
        onFocus={onFocus}
        onBlur={onBlur}
        onChange={onChange}
        value={value ? value : ''}
        disabled={disabled}
        autoComplete={type == 'password' ? 'new-password' : undefined}
      />}
      <span className="error">{getError()}</span>
    </div>
  );
};

