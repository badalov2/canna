import React, { useState, useEffect } from 'react';
import Link from "next/link";
import { Container } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useRouter } from 'next/router';

import { Main } from 'layout/main';
import { Header } from 'layout/header';
import { TextField } from 'components/TextField';
import { Button } from 'components/Button';
//import {GoogleIcon} from '../../../assets/img/icons';
import { validHandler } from 'utils/validation';
import { userLogin, userErrorAuth } from 'redux/actions/userActions';

import styles from './sign-in.module.scss';
import { AppState } from 'redux/store';

const SignIn = () => { 
  const [errors, setErrors] = useState({});
  const [lockForm, setLockForm] = useState(false);
  const [needValid, setNeedValid] = useState(false);
  const [fields, setFields] = useState({
    email: '',
    password: ''
  });

  const history = useRouter();
  const dispatch = useDispatch();
  const isAuth = useSelector<AppState>(state => state.user!.isAuth);
  const errorAuth = useSelector<AppState>(state => state.user!.errorAuth);

  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    const f = { ...fields };
    f[e.target.name as keyof typeof f] = e.target.value;
    setFields(f);
  };

  useEffect(() => {
    dispatch(userErrorAuth(false));
    if (needValid) {
      const validation = ['email', 'password'];
      const { errors } = validHandler(fields, validation);
      setErrors(errors);
    }
  }, [fields, dispatch, needValid]);

  useEffect(() => {
    if (isAuth) {
      history.push('/');
    }
  }, [isAuth, history]);

  useEffect(() => {
    setLockForm(false);
  }, [errorAuth]);

  const submitForm = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const { valid, errors } = validHandler(fields, ['email', 'password']);
    setErrors(errors);
    if (!valid) {
      setNeedValid(true);
    } else {
      setLockForm(true);
      dispatch(
        userLogin({
          email: fields['email'],
          password: fields['password']
        })
      );
    }
  };

  return (
    <div className={styles.signin}>
      <Header page="signin" />
      <Main className={styles.main}>
        <Container className={styles.container}>
          <h1>Welcome Back</h1>
          <p className={styles.subtitle}>Enter your credentials to access your account</p>
          <div className={styles["form-block"]}>
            <form action="" onSubmit={submitForm}>
              <div className={styles['form-block']}>
                <TextField
                  id="email"
                  type="email"
                  fieldName="E-mail"
                  value={fields['email']}
                  errors={errors}
                  disabled={lockForm}
                  onChange={changeHandler}
                />
                <TextField
                  id="password"
                  type="password"
                  fieldName="Password"
                  value={fields['password']}
                  errors={errors}
                  disabled={lockForm}
                  onChange={changeHandler}
                />
                {errorAuth && <span className={styles.error}>Wrong login or password</span>}
                <div className={styles["signin__button-group"]}>
                  <Button
                    type="submit"
                    color="green"
                    disabled={lockForm}
                  >
                    Login
                  </Button>
                  <div className={styles["signin__forgot-link"]}>
                    <Link href="/reset-password">Forgot Password?</Link>
                  </div>
                </div>

                {/* <div className="signin__social-signin">
                  <span>
                    or login with:
                  </span>
                  <Button
                    type="submit"
                    color="black"
                  >
                    <GoogleIcon />Google Account
                  </Button>
                </div> */}
              </div>
            </form>
          </div>
        </Container>
      </Main>
    </div>
  );
};

export default SignIn;