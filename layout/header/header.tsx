import { FC } from 'react';
import Link from "next/link";
import Image from "next/image";

import logo from '../../assets/img/logo/logotype.svg';

import styles from './header.module.scss';

export interface HeaderProps {
  page: 'signin' | 'signup'
}

export const Header: FC<HeaderProps> = ({ page }) => {
  return (
    <div className={styles.header}>
      <Image src={logo} alt="Canna Source Direct" />
      <div className={styles.header__nav}>
        {page == 'signin' &&
          <>
            Don’t have an account? -{' '}
            <Link href="/sign-up">Register here</Link>
          </>
        }
        {page == 'signup' &&
          <>
            Already have an account? -{' '}
            <Link href="/sign-in">Login</Link>
          </>
        }
      </div>
    </div>
  );
};
