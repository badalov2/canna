import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers/index";
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { userWatcher } from '../store/sagas';

const saga = createSagaMiddleware();
const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(
            saga
        )
    )
)

saga.run(userWatcher);

export default store;

export type AppState = ReturnType<typeof store.getState>;