export const dateFormat = date => {
  const options = {year: 'numeric', month: '2-digit', day: '2-digit'};
  return date ? new Date(date).toLocaleDateString('en-US', options) : null;
};

export const dateTimeFormat = date => {
  const options = {year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'};
  return date ? new Date(date).toLocaleDateString('en-US', options) : null;
};

export const dateFormatTimestmp = timestamp => {
  const options = {year: 'numeric', month: '2-digit', day: '2-digit'};
  return timestamp ? new Date(timestamp * 1000).toLocaleDateString('en-US', options) : null;
};


export const priceFormat = price => {
  return price ? price.toLocaleString('en-US') : null;
};